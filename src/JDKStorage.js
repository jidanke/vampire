/**
 * Created by Alex on 2015/1/31.
 * 1、定义了部分工具函数；
 * 2、设计了存储模块，在cc.sys.localStorage的基础上增加了加密、buffer功能支持。
 */

if (cc.sys.isNative) {
    require("src/md5.js");
    require("src/aes.js");
}

var jdk = jdk || {};

/**
 * 支持字符串格式化的cc.log的替代函数。
 * @example
 * <p>
 *     jdk.log("{0} is good, but {1} is better! {0} {2}", "cocos2d", "cocos2d-js");
 * </p>
 * @type {function}
 */
jdk.log = function () {
    var args = arguments;
    var formatted = args[0];

    if (args.length > 1) {
        formatted = args[0].replace(/{(\d+)}/g, function (match, number) {
            var index = parseInt(number) + 1;
            /** typeof number == "string" */
            return typeof args[index] != "undefined" ? args[index] : match;
        });
    }

    cc.log("jdk.log: " + formatted);
};


/**
 * 将指定对象的属性取出来格式化成字典字符串。
 * @example
 * <p>
 *     var obj = {a: 1, b: 2, c: 3};
 *     var dictString = jdk.toDictString(obj, "a", "c"); //dictString: {"a": 1, "c": 3}
 * </p>
 * @type {function}
 */
jdk.toDictString = function () {
    var args = arguments;
    if (args.length > 1 && typeof args[0] == "object") {
        var obj = args[0];
        var dict = {};
        for (var i = 1; i < args.length; i++) {
            if (args[i] in obj) {
                dict[args[i]] = typeof obj[args[i]] == "function" ? "function" : obj[args[i]];
            } else {
                dict[args[i]] = "undefined";
            }
        }

        return JSON.stringify(dict);
    }

    return args[0];
};


/**
 * 执行该函数可以为类型添加一些增强型的方法
 * @type {function}
 */
jdk.enhance = function () {
    if (!String.prototype.format) {
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != "undefined" ? args[number] : match;
            });
        };
    }
};


/**
 * GUID生成函数
 * @type {function}
 */
jdk.guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
})();


/**
 * 用户存储缓存，每次将缓存修改保存到buffer中直到调用flush方法才执行写入存储操作。
 * @type {class}
 */
jdk.UserStorageBuffer = (function () {
    var buffer = {};
    var dirty = false;
    var confuseSecret = "&^%$er4@WE^";

    var addConfusion = function (value) {
        return CryptoJS.AES.encrypt(String(value), confuseSecret);
    };

    var removeConfusion = function (confusedValue) {
        return CryptoJS.AES.decrypt(confusedValue, confuseSecret).toString(CryptoJS.enc.Utf8);
    };

    var scheduleFunc = null;

    var storage = {
        _salt: null,
        _secret: "90H*L$^%RJS#@",

        initialize: function (encryption) {
            if (this._salt === null) {
                var saltName = CryptoJS.MD5("salt");
                var salt = cc.sys.localStorage.getItem(saltName);

                if (salt === null || salt == "") {
                    salt = jdk.guid();
                    cc.sys.localStorage.setItem(saltName, salt);
                }

                this._salt = salt;
            }

            if (!encryption) {
                this._secret = null;
            }
        },

        _wrapKey: function (value) {
            if (this._salt !== null) {
                return CryptoJS.MD5(value + this._salt);
            }
        },

        _decrypt: function (encrypted) {
            return this._secret != null ? CryptoJS.AES.decrypt(encrypted, this._secret).toString(CryptoJS.enc.Utf8) : encrypted;
        },
        
        _encrypt: function (value) {
            return this._secret != null ? CryptoJS.AES.encrypt(String(value), this._secret) : value;
        },

        getItem: function (key) {
            var value = null;
            if (key !== null && key !== undefined) {
                key = this._wrapKey(key);
                value = cc.sys.localStorage.getItem(key);

                if (value !== null && value !== undefined) {
                    value = this._decrypt(value);
                }
            }

            return value;
        },

        setItem: function (key, value) {
            if (key !== null && key !== undefined && value !== null && value !== undefined) {
                key = this._wrapKey(key);
                value = this._encrypt(value);
                cc.sys.localStorage.setItem(key, value);
            }
        },

        removeItem: function (key) {
            if (key !== null && key !== undefined) {
                key = this._wrapKey(key);
                cc.sys.localStorage.removeItem(key);
            }
        }
    };

    var storageBuffer = new (cc.Class.extend({

        /**
         * 获取指定key对应的value。 @note 由于key不存在时返回""，所以存储""值时尤其注意区分。value=""时也无法将缓存value。
         * @type {function}
         * @param key
         * @returns {string} 若对应的key不存在则返回""，否则返回存储的value。
         */
        getItem: function (key) {
            if (key in buffer) {
                return buffer[key].deleted ? "" : removeConfusion(buffer[key].value);
            }

            var value = storage.getItem(key);
            //NOTE: value=""或者key不存在都会导致无法缓存
            if (value !== "") {
                buffer[key] = {value: value, dirty: false, deleted: false};
            }

            return removeConfusion(value);
        },

        /**
         * 将指定的key和value写入存储。
         * @type {function}
         * @param key
         * @param value
         */
        setItem: function (key, value) {
            if (key != null)/**key !== null && key !== undefined**/{
                value = addConfusion(value);
                if (key in buffer && !buffer[key].deleted && buffer[key].value == value) {
                    return;
                }
                buffer[key] = {value: value, dirty: true, deleted: false};
                dirty = true;
            }
        },

        /**
         * 删除指定的key。
         * @type {function}
         * @param key
         */
        removeItem: function (key) {
            buffer[key] = {value: null, dirty: true, deleted: true};
            dirty = true;
        },

        /**
         * 清理buffer
         * @type {function}
         */
        clean: function () {
            buffer = {};
            dirty = false;
        },

        /**
         * 将buffer中的脏数据flush到存储中。
         * @type {function}
         * @returns {Number} 执行flush操作的key数量。
         */
        flush: function () {
            var count = 0;

            if (dirty) {
                var deletedKeys = [];
                for (var key in buffer) {
                    var o = buffer[key];
                    if (o.deleted) {
                        storage.removeItem(key);
                        deletedKeys.push(key);
                        count++;
                    } else if (o.dirty) {
                        storage.setItem(key, o.value);
                        o.dirty = false;
                        count++;
                    }
                }

                for (var i in deletedKeys) {
                    delete buffer[deletedKeys[i]];
                }

                dirty = false;
            }
            return count;
        },

        /**
         * 注册自动flush buffer定时器
         * @type {function}
         * @param interval
         */
        scheduleFlush: function (interval) {
            if (interval == undefined) {
                interval = 10;
            }

            if (scheduleFunc == null) {
                var self = this;
                scheduleFunc = function () {
                    jdk.log("Called scheduleFunc function at: {0}.", new Date().getTime());
                    self.flush();
                };
                cc.director.getScheduler().scheduleCallbackForTarget(this, scheduleFunc, interval, cc.REPEAT_FOREVER, 0, false);
            } else {
                jdk.log("You can't repeatedly call scheduleFlush function until you has called unscheduleFlush function.");
            }
        },

        /**
         * 取消自动flush buffer定时器
         * @type {function}
         */
        unscheduleFlush: function () {
            if (scheduleFunc != null && typeof scheduleFunc == "function") {
                cc.director.getScheduler().unscheduleCallbackForTarget(this, scheduleFunc);
                scheduleFunc = null;
                jdk.log("Called unscheduleFlush function at : {0}.", new Date().getTime());
            }
        }
    }))();

    return storageBuffer;
})();