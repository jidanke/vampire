/**
 * Created by Alex on 2015/2/6.
 */

if (cc.sys.isNative) {
    require("src/JDKFileLoader.js")
}

var models = models || {};
var configFiles = configFiles || {
        monster: "res/monster.json",
        skill: "res/skill.json"
    };

jdk.registerJsonLoader();
for (var i in configFiles) {
    jdk.jsonLoader.load(configFiles[i], function (err, data) {
    });
}

/**
 * 获取指定id的技能配置数据
 * @param id
 */
function getSkillData(id) {
    var skills = jdk.jsonLoader.getJson(configFiles.skill);
    return skills ? skills[id] : null;
}


/**
 * 获取指定id的怪物配置数据
 * @param id
 */
function getMonsterData(id) {
    var monsters = jdk.jsonLoader.getJson(configFiles.monster);
    return monsters ? monsters[id] : null;
}


/**
 * 实体
 * @class
 * @extends cc.Class
 */
models.Entity = cc.Class.extend({
    /**
     * 定时器
     * @type {cc.Scheduler}
     */
    _scheduler: null,

    ctor: function () {
        this._scheduler = cc.director.getScheduler();
    },

    schedule: function (callback_fn, interval, repeat, delay) {
        interval = interval || 0;
        repeat = (repeat == null) ? cc.REPEAT_FOREVER : repeat;
        delay = delay || 0;

        this._scheduler.scheduleCallbackForTarget(this, callback_fn, interval, repeat, delay, false);
    },

    scheduleOnce: function (callback_fn, delay) {
        this.schedule(callback_fn, 0.0, 0, delay);
    },

    unschedule: function (callback_fn) {
        if (!callback_fn)
            return;

        this._scheduler.unscheduleCallbackForTarget(this, callback_fn);
    },

    unscheduleAllCallbacks: function () {
        this._scheduler.unscheduleAllCallbacksForTarget(this);
    }
});


/**
 * 技能
 * @type {class}
 */
models.Skill = models.Entity.extend({
    /**
     * id
     * @type {int}
     */
    id: null,

    /**
     * 名称
     * @type {string}
     */
    name: "",

    /**
     * 优先级
     * @type {int}
     */
    priority: 0,

    /**
     * 伤害值
     * @type {int}
     */
    damage: 0,

    /**
     * 是否是治疗技能
     */
    isHealing: false,

    /**
     * 冷却时间(毫秒)
     * @type {int}
     */
    coolDownTime: 0,

    /**
     * 技能冷却时间变化时回调
     * @type {function}
     */
    coolDownTimeChangedCallback: null,

    /**
     * 是否处于技能冷却状态
     * @type {boolean}
     */
    _skillCDEnable: false,

    /**
     * 上次释放技能的时间
     * @type {int}
     */
    _lastFiredTime: null,

    /**
     * 初始化函数
     * @param id
     */
    ctor: function (id) {
        var skillData = getSkillData(id);
        if (!skillData) {
            throw "Not found skill(id): " + id;
        }

        this.id = id;
        this.name = String(skillData.name);
        this.priority = parseInt(skillData.priority);
        this.damage = parseInt(skillData.damage);
        this.isHealing = Boolean(skillData.isHealing);
        this.coolDownTime = parseFloat(skillData.coolDownTime) * 1000;
        this._super();
    },

    /**
     * 技能是否正处于冷却状态
     * @returns {boolean}
     */
    coolDownTimeEnable: function () {
        return this._skillCDEnable;
    },

    /**
     * 筛选攻击对象(技能冷却时或者是治疗技能返回null)
     * @param attackBodies
     */
    selectTargets: function (attackBodies) {
        if (this.isHealing) {
            return null;
        }

        //TODO:如果需要，这里要根据技能不同对攻击对象进行筛选
        return [attackBodies[0]];
    },

    /**
     *  释放技能
     *  @return {int} 技能伤害值
     */
    fire: function () {
        if (this._skillCDEnable) {
            return 0;
        }

        this._lastFiredTime = new Date().getTime();
        this._skillCDEnable = true;
        this._triggerCoolDownTimeChangedCallback(this.coolDownTime);
        this.schedule(this._changeCoolDownTime, 0, cc.REPEAT_FOREVER, 0);
        return this.damage;
    },

    /**
     * 修改技能冷却时间
     * @private
     */
    _changeCoolDownTime: function () {
        if (this._skillCDEnable) {
            var interval = new Date().getTime() - this._lastFiredTime;
            var remaining = this.coolDownTime - interval;
            remaining = remaining > 0 ? remaining : 0;
            if (remaining == 0) {
                this.unschedule(this._changeCoolDownTime);
                this._skillCDEnable = false;
                this._lastFiredTime = null;
            }

            this._triggerCoolDownTimeChangedCallback(remaining);
        }
    },

    /**
     * 触发冷却倒计时回调函数
     * @param remaining
     * @private
     */
    _triggerCoolDownTimeChangedCallback: function (remaining) {
        this.coolDownTimeChangedCallback && this.coolDownTimeChangedCallback(remaining, this.coolDownTime);
    }
});


/**
 * 攻击体
 * @type {class}
 */
models.AttackBody = models.Entity.extend({
    /**
     * 血量/生命值
     * @type {int}
     */
    _hp: 0,

    /**
     * 血量/生命值上限
     * @type {int}
     */
    maxHP: 0,

    /**
     * 闪避率
     * @type {int}
     */
    dodgeProb: 0,

    /**
     * 暴击率
     * @type {int}
     */
    criticalProb: 0,

    /**
     * 死亡时回调
     */
    deadCallback: null,

    /**
     * 血量/生命值发生变化时回调
     */
    HPChangedCallback: null,

    /**
     * 初始化函数
     * @param {int} maxHP
     * @param {int} hp
     * @param {int} dodgeProb
     * @param {int} criticalProb
     */
    ctor: function (maxHP, hp, dodgeProb, criticalProb) {
        this._super();
        this.maxHP = maxHP;
        this.dodgeProb = dodgeProb;
        this.criticalProb = criticalProb;
        this._hp = hp;
    },

    /**
     * 怪物是否死亡
     * @returns {boolean}
     */
    isDead: function () {
        return this._hp <= 0;
    },

    /**
     * 获取当前血量/生命值
     * @returns {int}
     */
    getHP: function () {
        return this._hp;
    },

    /**
     * 加血/生命值
     * @param value
     * @returns {int}
     */
    subtractHP: function (value) {
        if (this._hp > 0 && value > 0) {
            var hp = this._hp - value;
            hp = hp > 0 ? hp : 0;
            this._hp = hp;
            this.HPChangedCallback && this.HPChangedCallback(this._hp, this.maxHP);
            this._hp == 0 && this.deadCallback && this.deadCallback();
        }

        return this._hp;
    },

    /**
     * 减血/生命值
     * @param value
     * @returns {int}
     */
    addHP: function (value) {
        if (this._hp > 0 && this._hp < this.maxHP && value > 0) {
            var hp = this._hp + value;
            hp = hp > this.maxHP ? this.maxHP : hp;
            this._hp = hp;
            this.HPChangedCallback && this.HPChangedCallback(this._hp, this.maxHP);
        }

        return this._hp;
    },

    /**
     * 攻击
     * @param {models.AttackBody|Array} attackBodies 攻击对象
     * @param callback 攻击后回调 {function(attackTarget, attacker, skill, damage)}
     * @param automatic 是否自动攻击
     */
    fire: function (attackBodies, callback, automatic) {
        //hold place
    }
});


/**
 * 怪物
 * @type {class}
 */
models.Monster = models.AttackBody.extend({
    /**
     * id
     * @type {int}
     */
    id: null,

    /**
     * 名称
     * @type {string}
     */
    name: "",

    /**
     * 技能
     * @type {models.Skill}
     */
    skill: null,

    /**
     * 自动攻击的目标
     * @type {models.AttackBody}
     */
    _autoAttackTarget: null,

    /**
     * 初始化函数
     * @param id
     */
    ctor: function (id) {
        var monsterData = getMonsterData(id);
        if (!monsterData) {
            throw "Not found monster(id): " + id;
        }

        if (monsterData.skill != null) {
            this.skill = new models.Skill(monsterData.skill);
        }

        this.id = id;
        this.name = String(monsterData.name);
        this._super(
            parseInt(monsterData.maxHP),
            parseInt(monsterData.maxHP),
            parseInt(monsterData.dodgeProb),
            parseInt(monsterData.criticalProb));
    },

    /**
     * 攻击
     * @param {models.AttackBody|Array} attackBodies
     * @param {function} callback
     * @param {boolean} automatic
     */
    fire: function (attackBodies, callback, automatic) {
        if (this.skill.isHealing) {
            cc.warn("Monster has a healing skill. (id=" + this.id + ")");
            return;
        }

        var vampire = null;
        if (!(attackBodies instanceof Array)) {
            vampire = attackBodies;
        } else {
            vampire = attackBodies[0];
        }

        if (this._autoAttackTarget || vampire == null) {
            return;
        }

        if (automatic) {
            cc.log(this.name + ": start automatically attack...");
            this._autoAttackTarget = vampire;
            var self = this;
            this.schedule(this._fire.bind(self, vampire, callback), 0, cc.REPEAT_FOREVER, 0);
        } else {
            this._fire(vampire, callback);
        }
    },

    /**
     * 攻击
     * @param {models.AttackBody|Array} vampire
     * @param callback
     * @private
     */
    _fire: function (vampire, callback) {
        if (this._autoAttackTarget && this._autoAttackTarget !== vampire) {
            cc.error("{_fire: function(vampire, callback)}: Don't match the autoAttackTarget...");
            return;
        }

        if (this.isDead() || vampire.isDead()) {
            if (this._autoAttackTarget) {
                cc.log(this.name + ": end automatically attack...");
                this.unschedule(this._fire);
                this._autoAttackTarget = null;
            }
            return;
        }

        if (!this.skill.coolDownTimeEnable()) {
            var damage = this.skill.fire();

            if (Math.random() * 100 <= vampire.dodgeProb) {
                damage = 0;
//                cc.log('吸血鬼闪避');
            } else if (Math.random() * 100 <= this.criticalProb) {
                damage = damage * 2;
//                cc.log('怪物暴击');
            }

            if (damage > 0) {
                vampire.subtractHP(damage);
            }

            callback && callback(vampire, this, this.skill, damage);
        }
    }
});


/**
 * 吸血鬼
 * @type {class}
 */
models.Vampire = models.AttackBody.extend({
    /**
     * 名称
     * @type {string}
     */
    name: "",

    /**
     * 技能
     * @type {Array}
     */
    skills: null,

    /**
     * 自动攻击的对象
     * @type {Array}
     */
    _autoAttackTargets: null,

    /**
     * 初始化函数
     * @param name
     * @param maxHP
     * @param hp
     * @param dodgeProb
     * @param criticalProb
     * @param skillIds
     */
    ctor: function (name, maxHP, hp, dodgeProb, criticalProb, skillIds) {
        this._super(parseInt(maxHP), parseInt(hp), parseInt(dodgeProb), parseInt(criticalProb));
        this.name = name;

        if (!(skillIds instanceof Array)) {
            skillIds = [skillIds];
        }

        var skills = [];
        for (var i = 0; i < skillIds.length; i++) {
            skills.push(new models.Skill(skillIds[i]));
        }

        this.skills = skills;
    },

    /**
     * 攻击
     * @param {models.AttackBody} attackBodies
     * @param callback
     * @param automatic
     */
    fire: function (attackBodies, callback, automatic) {
        if (this._autoAttackTargets || attackBodies == null) {
            return;
        }

        if (!(attackBodies instanceof Array)) {
            attackBodies = [attackBodies];
        }

        if (automatic) {
            cc.log(this.name + ": start automatically attack...");
            this._autoAttackTargets = attackBodies;
            var self = this;
            this.schedule(this._fire.bind(self, attackBodies, callback), 0, cc.REPEAT_FOREVER, 0);
        } else {
            this._fire(attackBodies, callback);
        }
    },

    /**
     * 攻击
     * @param {models.AttackBody} attackBodies
     * @param callback
     * @private
     */
    _fire: function (attackBodies, callback) {
        if (this._autoAttackTargets && this._autoAttackTargets !== attackBodies) {
            cc.error("{_fire: function(attackBodies, callback)}: Don't match the autoAttackTargets...");
            return;
        }

        var availableAttackBodies = [];
        for (i = 0; i < attackBodies.length; i++) {
            if (!attackBodies[i].isDead()) {
                availableAttackBodies.push(attackBodies[i]);
            }
        }

        if (this.isDead() || availableAttackBodies.length == 0) {
            if (this._autoAttackTargets) {
                cc.log(this.name + ": end automatically attack...");
                this.unschedule(this._fire);
                this._autoAttackTargets = null;
            }
            return;
        }

        var availableSkill = this._getAvailableSkill();
        if (availableSkill == null) {
            return;
        }

        var damage = availableSkill.fire();
        if (availableSkill.isHealing) {
            this.addHP(damage);
            callback && callback(this, this, availableSkill, damage);
            return;
        }

        availableAttackBodies = availableSkill.selectTargets(availableAttackBodies);
        for (i = 0; i < availableAttackBodies.length; i++) {
            var currentTarget = availableAttackBodies[i];

            if (Math.random() * 100 <= currentTarget.dodgeProb) {
                damage = 0;
//                cc.log('怪物闪避');
            } else if (Math.random() * 100 <= this.criticalProb) {
                damage = damage * 2;
//                cc.log('吸血鬼暴击');
            }

            if (damage > 0) {
                currentTarget.subtractHP(damage);
            }

            callback && callback(currentTarget, this, availableSkill, damage);
        }
    },

    /**
     * 在所有可用技能中筛选一个优先级最高的技能
     * @returns {models.Skill}
     * @private
     */
    _getAvailableSkill: function () {
        var availableSkills = [];
        for (var i = 0; i < this.skills.length; i++) {
            if (!this.skills[i].coolDownTimeEnable()) {
                availableSkills.push(this.skills[i]);
            }
        }
        if (availableSkills.length == 0) {
            return null;
        }

        var skill = availableSkills[0];
        for (i = 1; i < availableSkills.length; i++) {
            if (skill.priority > availableSkills[i].priority) {
                skill = availableSkills[i];
            }
        }

        return skill;
    }
});

