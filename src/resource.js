var res = {
    HelloWorld_png : "res/HelloWorld.png",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    dabian_png : "res/dabian.png",
    img_jpg : "res/img.jpg",
    spacing_tmx : "res/spacing.tmx",
    spacing_png : "res/spacing.png",
    mask_tmx : "res/mask.tmx",
    mask_png : "res/mask.png",
    mask_r_png : "res/mask_r.png",
    yuan_png : "res/yuan.png",
    mv_jpg : "res/mv.jpg",
    yuan2_png : "res/yuan2.png",
    all_bg_01_png : "res/all_bg_01.png",
    all_bg_03_png : "res/all_bg_03.png",
    all_bg_04_png : "res/all_bg_04.png",
    cd_png : "res/cd.png"
    
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}