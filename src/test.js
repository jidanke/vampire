
var HelloWorldL = cc.Layer.extend({  
	ctor:function(){
		this._super();
		
//		var size = cc.winSize; 
		
//		var listView = new ccui.ScrollView();
//		// set list view ex direction
//		listView.setDirection(ccui.ScrollView.DIR_VERTICAL);
//		listView.setTouchEnabled(true);
//		listView.setBounceEnabled(true);
//		listView.setBackGroundImage(res.all_bg_04_png);
//		listView.setBackGroundImageScale9Enabled(true);
////		listView.setContentSize(cc.size(240, 400));
//		listView.setContentSize(cc.size(300, 30*9));
//		listView.setAnchorPoint(cc.p(0.5,0.5));
//		listView.setPosition(size.width/2, size.height/2);
//		listView.setInnerContainerSize(cc.size(800, 80*9));
//		
//		this.addChild(listView);
//		for(var i =0; i < 9; i++){
//			var sprite = new cc.Sprite(res.all_bg_03_png);
//			listView.addChild(sprite);
//			sprite.x = listView.width/2;
//			sprite.y= listView.getInnerContainerSize().height + 40 - (i+1)*80;
//			sprite.setAnchorPoint(cc.p(0.5,0.5));
//		}
		
		
//		var minusSprite = cc.Sprite(res.all_bg_03_png);
//		var plusSprite = cc.Sprite(res.all_bg_04_png);
//		var stepper = cc.ControlStepper.create(minusSprite, plusSprite);
//		stepper.setPosition(size.width/2, size.height/2);
////		stepper.addTargetWithActionForControlEvents(this, this.valueChanged, cc.CONTROL_EVENT_VALUECHANGED);
//		this.addChild(stepper);
////		var root = ccs.uiReader.widgetFromJsonFile(res.MainScene_json);
////		this.addChild(root);
		
		var winSize = cc.Director.getInstance().getWinSize();

		// Parent layer for better positioning
		var layer = cc.Node();
		layer.setPosition(winSize.width / 2, winSize.height / 2);
		this.addChild(layer, 1);
		var layer_width = 0;

		// Add the black background for the text
		var background = cc.Scale9Sprite(res.all_bg_01_png);
		background.setContentSize(100, 50);
		background.setPosition(layer_width + background.getContentSize().width / 2.0, 0);
		layer.addChild(background);
		
		
		
		this._displayValueLabel = new cc.LabelTTF("0", "HelveticaNeue-Bold", 30);

		this._displayValueLabel.setPosition(background.getPosition());
		layer.addChild(this._displayValueLabel);
		
//		var bb = new cc.LabelTTF("*", "HelveticaNeue-Bold", 30);
//		bb.setPosition(100, 100);
//		this.addChild(bb);
		
		layer_width += background.getContentSize().width;

		// Create stepper with minus and plus image
		var minusSprite = cc.Sprite(res.CloseNormal_png);
		var plusSprite = cc.Sprite(res.CloseSelected_png);
		var stepper = cc.ControlStepper.create(minusSprite, plusSprite);
//		stepper.minusLabel = bb;
		stepper.maxValue = 10;
		
//		stepper.plusSLabel = new cc.LabelTTF("=", "HelveticaNeue-Bold", 30);
		
		stepper.setPosition(layer_width + 10 + stepper.getContentSize().width / 2, 0);
		stepper.addTargetWithActionForControlEvents(this, this.valueChanged, cc.CONTROL_EVENT_VALUECHANGED);
		
		
		layer.addChild(stepper);

		layer_width += stepper.getContentSize().width;

		// Set the layer size
//		layer.setContentSize(layer_width, 0);
		layer.setAnchorPoint(0.5, 0.5);
		
		// Update the value label
//		this.valueChanged(stepper, cc.CONTROL_EVENT_VALUECHANGED);
		
		
	},
	valueChanged:function (sender, controlEvent) {
		// Change value of label.
		this._displayValueLabel.setString(sender.getValue().toString());
	}
	
});
//var UILabelTest = UIScene.extend({
//	init: function () {
//		if (this._super()) {
//			//init text
//			this._topDisplayLabel.setString("");
//			this._bottomDisplayLabel.setString("Label");
//
//			// Create the label
//			var text = new ccui.Text();
//			text.attr({
//				string: "Label",
//				font: "30px AmericanTypewriter",
//				x: this._widget.width / 2,
//				y: this._widget.height / 2 + text.height / 4
//			});
//			this._mainNode.addChild(text);
//
//			return true;
//		}
//		return false;
//	}
//});
var HelloWorld = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldL();
        this.addChild(layer);
        
        
//        this.ALayer.setVisible(false);
    }
});

