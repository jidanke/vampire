
var HelloWorldLayer = cc.Layer.extend({
	b_num:['0','5','10','15','20','25','30','35','40','45','50','60','70','80','90'],
	hero:null,
	cameraStatue:null,
	heroSpeed:32,
	tiled:null,
	layer_bb:null,
	flagshit:true,
	flag:true,
	sacle:1,
	RMaps:null,
	mask_tmx:null,
	MMaps:null,
	ctor:function () {  
		this._super();
		this.setScale(this.sacle);
	
		var size = cc.winSize;  
		cc.director.setProjection(cc.Director.PROJECTION_2D);
		//配置
		var initConfigArray = [{name: 'obstacles', cls: obstaclesClass, cb: function(){
				cc.log("障碍物");
			} 
		},{name: 'wood', cls: WoodBattleClass, cb: [function(obj){
				cc.log("木材战斗点");
			},function(obj){
				cc.log("木材点");
			},function(obj){
				cc.log("捡完木材");
			}]
		},{name: 'cloth', cls: ClothBattleClass, cb: [function(obj){
				cc.log("布料战斗点");
			},function(obj){
				cc.log("布料点");
			},function(obj){
				cc.log("捡完布料");
			}]
		}];
		
		//地图资源
		this.RMaps = new maps.Resources(res.spacing_tmx, function(){
			cc.log("随机遇到了敌人");
		}, initConfigArray, [[3,2]], this.b_num);
		this.tiled = this.RMaps.getTiled();
		this.addChild(this.tiled);
		
		//地图迷雾
		this.MMaps = new maps.Fog(res.mask_tmx);
		this.mask_tmx = this.MMaps.getTiled();
		this.addChild(this.mask_tmx);
		
		//吸血鬼
		this.hero = new cc.Sprite(res.dabian_png);  
		this.hero.setPosition(32, 32);
		this.hero.setAnchorPoint(0,0);
		this.tiled.addChild(this.hero);
		this.setPosition(cc.p(size.width/2*this.sacle - this.hero.getPositionX(), size.height/2*this.sacle - this.hero.getPositionY()));
		
		
		var selfA = this;
		
		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches:true,
			onTouchBegan: function (touch, event) {
				if(selfA.flag){
					selfA.flag = false;
					var posX = selfA.hero.getPositionX();
					var posY = selfA.hero.getPositionY();
					var speedmove = 0.4;
					
					
					
					if(size.width/2 - touch.getLocation().x > 32 && touch.getLocation().y < size.height/2 + 50 && touch.getLocation().y > size.height/2 - 50){
//						cc.log('left');
						var x = (posX - selfA.heroSpeed)/selfA.heroSpeed;
						var y = posY/selfA.heroSpeed;
						//逻辑判断
						if(selfA.RMaps.isCanMove(x, y)){
							//英雄移动
							selfA.hero.setPositionX(posX - selfA.heroSpeed);
							posX = selfA.hero.getPositionX();
							//背景移动
							selfA.moveBackTiled(speedmove, selfA.heroSpeed*selfA.sacle, 0);
							
							var obj = selfA.RMaps.getCellObj(x, y);
							obj && obj.doSomething();
						}else{
							selfA.flag = true;
						}
						
																				
					}else if(touch.getLocation().x - size.width/2 > 32 && touch.getLocation().y < size.height/2 + 50 && touch.getLocation().y > size.height/2 - 50){
//						cc.log('right');
						var x = (posX + selfA.heroSpeed)/selfA.heroSpeed;
						var y = posY/selfA.heroSpeed;
						//逻辑判断
						if(selfA.RMaps.isCanMove(x, y)){
							//英雄移动
							selfA.hero.setPositionX(posX + selfA.heroSpeed);
							posX = selfA.hero.getPositionX();

							//背景移动
							selfA.moveBackTiled(speedmove, -(selfA.heroSpeed*selfA.sacle), 0);

							var obj = selfA.RMaps.getCellObj(x, y);
							obj && obj.doSomething();
						}else{
							selfA.flag = true;
						}

					}else if(size.height/2 - touch.getLocation().y > 32 && touch.getLocation().x < size.width/2 + 50 && touch.getLocation().x > size.width/2 - 50){
//						cc.log('down');
					
						var x = posX/selfA.heroSpeed;
						var y = (posY - selfA.heroSpeed)/selfA.heroSpeed;

						//逻辑判断
						if(selfA.RMaps.isCanMove(x, y)){
							//英雄移动
							selfA.hero.setPositionY(posY - selfA.heroSpeed);
							posY = selfA.hero.getPositionY();

							//背景移动
							selfA.moveBackTiled(speedmove, 0, selfA.heroSpeed*selfA.sacle);
							
							var obj = selfA.RMaps.getCellObj(x, y);
							
							obj && obj.doSomething();
						}else{
							selfA.flag = true;
						}
						

					}else if(touch.getLocation().y - size.height/2 > 32 && touch.getLocation().x < size.width/2 + 50 && touch.getLocation().x > size.width/2 - 50){
//						cc.log('top');
						var x = posX/selfA.heroSpeed;
						var y = (posY + selfA.heroSpeed)/selfA.heroSpeed;

						//逻辑判断
						if(selfA.RMaps.isCanMove(x, y)){
							//英雄移动
							selfA.hero.setPositionY(posY + selfA.heroSpeed);
							posY = selfA.hero.getPositionY();

							//背景移动
							selfA.moveBackTiled(speedmove, 0, -(selfA.heroSpeed*selfA.sacle));

							var obj = selfA.RMaps.getCellObj(x, y);
							obj && obj.doSomething();
						}else{
							selfA.flag = true;
						}
						
					}else{
						selfA.flag = true;
					}
					
					var px = posX/selfA.heroSpeed;
					var py = 49 - posY/selfA.heroSpeed;
					
					selfA.MMaps.setFog(px, py);
				
					
		
				}
				
				return true;
			}
		}, this);
		
		
//		this.scheduleUpdate();
		
		return true;  
	},
	moveBackTiled:function(s, x, y){
		var selfA = this;
		selfA.runAction(
				cc.Sequence.create(cc.MoveBy.create(s, cc.p(x, y)),cc.CallFunc.create(function(){
				selfA.flag = true;
			}, this))
		);
	}

});  
var ceshilayer = cc.Layer.extend({
	helloA:null,
	ctor:function () {  
		this._super();  
		var size = cc.winSize;  
		
		var clipper = new cc.ClippingNode();
		this.addChild(clipper);
		var yellowBox = new cc.LayerColor(cc.color(0,0,0,255), size.width, size.height);
		clipper.addChild(yellowBox);
		
		var spriteA = new cc.Sprite(res.yuan_png);
		spriteA.setPosition(size.width/2+16, size.height/2+16);
		spriteA.scale = 1.3;
		
		var spriteB = new cc.Sprite(res.yuan2_png);
		spriteB.setPosition(size.width/2+16, size.height/2+16);
		spriteB.scale = 1.3;
		this.addChild(spriteB);
		
		clipper.addChild(spriteA);
		clipper.setStencil(spriteA);
		clipper.setInverted(true);
		clipper.setAlphaThreshold(0);
		
//		var act1 = cc.ScaleTo.create(1, 1.5);
//		var act2 = cc.ScaleTo.create(1, 1.3);
//		var sq1 = cc.Sequence.create(act1, act2);
//		spriteA.runAction(sq1.repeatForever());
//		
//		
//		var act3 = cc.ScaleTo.create(1, 1.5);
//		var act4 = cc.ScaleTo.create(1, 1.3);
//		var sq2 = cc.Sequence.create(act3, act4);
//		
//		spriteB.runAction(sq2.repeatForever());
		
		return true;  
	}
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
        
        var cslayer = new ceshilayer();
        this.addChild(cslayer);
        
//        this.ALayer.setVisible(false);
    }
});

