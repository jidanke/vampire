var fightLayer = cc.Layer.extend({
	_enemy_num:null,
	_enemy_arr:[],
	_enemy_x:null,
	_enemy_y:null,
	_protagonis:null,
	_MA_NUM:0,
	_MAAll:null,
	ctor:function(enemy_num) {  
		this._super();
		
		var size = cc.winSize;  
		this._enemy_num = 2;
		
        this._createBackground();
        
        if(this._enemy_num == 1){
        	this._enemy_x = size.width/2 - 80;
        }else if(this._enemy_num == 2){
        	this._enemy_x = size.width/2 - 200;
        }else if(this._enemy_num == 3){
        	this._enemy_x = size.width/2 - 300;
		}
        this._enemy_y = size.height/2 + 300;
  
        // 吸血鬼
        var NPL = new newPL(new models.Vampire("吸血鬼", 300, 300, 30, 20, [2,4]), size.width/2 - 80, 300);
        
        // 怪物
        this._MAAll = [];
        for(var i = 1; i <= 2;i++){
        	var NMA = new newMA(new models.Monster(i), this._enemy_x, this._enemy_y);
        	this._MAAll.push(NMA);
        	
        	if(this._enemy_num != 1){
        		this._enemy_x += 210;
        	}
        }

        NPL.init(this._MAAll);
        NPL.fire();
        this.addChild(NPL);
        
        for(var i = 0; i < 2;i++){
        	this._MA_NUM ++;
        	this._MAAll[i].init(NPL);
        	this._MAAll[i].fire();
        	this._MAAll[i].deadCallback = this._deadCallback.bind(this,i);
        	this.addChild(this._MAAll[i]);
        }

		return true;  
	},
    _createBackground:function(){
        var size = cc.winSize;
        var spriteB = new cc.Sprite(res.all_bg_01_png);
        spriteB.setPosition(size.width/2, size.height/2);
        this.addChild(spriteB);
    },
    _deadCallback:function(i){
    	this._MA_NUM --;
    	cc.log(this._MA_NUM);
    	if(this._MA_NUM == 0){
    		this._MAAll[i]._PL.CD_flag = false;
    		this._MAAll[i].CD_flag = false;
    		var hws = new HelloWorldScene();
    		cc.director.runScene(new cc.TransitionFade(1,hws));
    	}
    },
    _bindDTCC:function(){
    	for(var i=0; i < this._vampire.skills.length;i++){

    		var skill = this._vampire.skills[i];
    		skill.coolDownTimeChangedCallback = this._coolDownTimeChangedCallback.bind(this, i, cdp);
    	}
    },
});


// 怪物
var newMA = cc.Layer.extend({
	_monster: null,
	_PL:null,
	sprit: null,
	_width_b:null,
	_height_b:null,
	_bg:null,
	_b_bg:null,
	_cd_bg:null,
	_createImg:null,
	CD_flag:true,
	deadCallback:null,
	ctor: function(monster, width_b, height_b){
		this._super();
		this._monster = monster;
		this._width_b = width_b;
		this._height_b = height_b;
		// 血条
		this._createBlood(this._width_b, this._height_b);
		// 形象
		this._createImg(this._width_b, this._height_b);
		
		// 技能CD
		this._createCD(this._width_b, this._height_b);
	},
	_coolDownTimeChangedCallback:function (remaining, coolDownTime){
		if(this.CD_flag){
			this._setReduceCD(remaining/coolDownTime);
			if(remaining == 0){
	// jdk.log("技能：{0} 施放者 : {1}", this.monster.skill.name, this.monster.name);
			}
		}
	},
	_bindDTCC:function(){
		this._monster.skill.coolDownTimeChangedCallback = this._coolDownTimeChangedCallback.bind(this);
		
	},
	_HPChangedCallback:function(hp, maxHp){
// jdk.log("name:{0} hp={1} maxHP={2}", this._monster.name, hp, maxHp);
		this._setReduceBlood(hp/maxHp);
		this._putInjured();
		if(hp == 0){
			// cc.log("我擦，"+this._monster.name+" 挂了");
			this.deadCallback && this.deadCallback();
			this.setVisible(false);
			// if(globalNum == 2){
			// cc.log(globalNum);
			// // for(var i=0; i < this._PL._vampire.skills.length;i++){
			// //// this._PL._vampire.skills[i]._skillCDEnable = false;
			// //
			// this._PL._vampire.skills[i].unschedule(this._PL._vampire.skills[i]._changeCoolDownTime);
			// // }
			// }
		}
	},
	_bindHPC:function(){
		this._monster.HPChangedCallback = this._HPChangedCallback.bind(this);
	},
	fire:function(){
		var selfA = this;
		this._monster.fire(this._PL._vampire, function (attackTarget, attacker, skill, damage) {
			if(damage == 0){
				// NPL.runAction(
				// cc.Sequence.create(cc.MoveBy.create(0.5, cc.p(0, -5)))
				// );
				// cc.log("我擦，吸血鬼超级闪避");
				selfA._PL._injured();
				// cc.log(attackTarget.name+"------");
			}
//			 jdk.log("attackTarget={0} attacker={1} skill={2} damage={3}",
//			 attackTarget.name, attacker.name, skill.name, damage);
		}, true);
	},
	_createBlood:function(width_b, height_b){
		this._bg = new cc.Sprite(res.all_bg_04_png);
		this._bg.setPosition(width_b - 1, height_b);
		this._bg.setAnchorPoint(0, 0.5);
		this.addChild(this._bg);

		this._b_bg = new cc.Sprite(res.all_bg_03_png);
		this._b_bg.setPosition(width_b, height_b);
		this._b_bg.setAnchorPoint(0, 0.5);
// this.b_bg.setScaleX(1);
		this.addChild(this._b_bg);
	},
	_setReduceBlood:function(num){
		this._b_bg.setScaleX(num);
		var act1 = cc.FadeTo.create(0.3, 100);
		var act2 = cc.FadeTo.create(0.3, 255);
		var sq1 = cc.Sequence.create(act1,act2);
		this._b_bg.runAction(sq1);
	},
	_createCD:function(width_b, height_b){
		this._cd_bg = new cc.Sprite(res.cd_png);
		this._cd_bg.setPosition(width_b + 55, height_b - 200);
		this._cd_bg.setAnchorPoint(0, 0.5);

		this.addChild(this._cd_bg);
	},
	_setReduceCD:function(num){
		this._cd_bg.setScaleX(num);

	},
	_createImg:function(width_b, height_b){

		this._img_bg = new cc.Sprite(res.img_jpg);
		this._img_bg.setPosition(width_b + 90, height_b - 100);
		this.addChild(this._img_bg);
	},
	_injured:function(){
		var act1 = cc.FadeTo.create(0.3, 100);
		var act2 = cc.FadeTo.create(0.3, 255);
		var sq1 = cc.Sequence.create(act1,act2);
		this._img_bg.runAction(sq1);
	},
	_putInjured:function(){
		var act1 = cc.ScaleTo.create(0.1, 0.90);
		var act2 = cc.ScaleTo.create(0.1, 1);
		var act3 = cc.ScaleTo.create(0.1, 0.90);
		var act4 = cc.ScaleTo.create(0.1, 1);
		var sq1 = cc.Sequence.create(act1, act2, act3, act4);
		this._img_bg.runAction(sq1);
	},
	init:function(PL){
		this._PL = PL;
		this._bindDTCC();
		this._bindHPC();
	}
});

// 吸血鬼
var newPL = cc.Layer.extend({
	_monsterAll: null,
	_vampire:null,
	_width_b:null,
	_height_b:null,
	_bg:null,
	_b_bg:null,
	_createImg:null,
	_CD_w:0,
	CD_flag:true,
	ctor: function(vampire, width_b, height_b){
		this._super();
		this._vampire = vampire;
		this._width_b = width_b;
		this._height_b = height_b;
		// 血条
		this._createBlood(this._width_b, this._height_b);
		// 形象
		this._createImg(this._width_b, this._height_b);
	},
	_coolDownTimeChangedCallback:function (skill_number, cdp, remaining, coolDownTime){
		if(this.CD_flag){
			this._setReduceCD(cdp, remaining/coolDownTime);
			if(remaining == 0){
// jdk.log("技能：{0} 施放者 : {1}", this._vampire.skills[skill_number].name,
// this._vampire.name);
			}
		}
		
	},
	_bindDTCC:function(){
		for(var i=0; i < this._vampire.skills.length;i++){
			
			this._CD_w+=80;
			
			var cdp = new cc.Sprite(res.cd_png);
			this._createCD(cdp, this._width_b - this._CD_w, this._height_b);
			var skill = this._vampire.skills[i];
			skill.coolDownTimeChangedCallback = this._coolDownTimeChangedCallback.bind(this, i, cdp);
			// var self = this;
			// (function(k){ return function(remaining,
			// coolDownTime){self._coolDownTimeChangedCallback(k, remaining,
			// coolDownTime);} })(i);
		}
	},
	_HPChangedCallback:function(hp, maxHp){
// jdk.log("name:{0} hp={1} maxHP={2}", this._vampire.name, hp, maxHp);
		this._setReduceBlood(hp/maxHp);
		this._putInjured();
		
		if(hp == 0){
// cc.log("我擦，"+this._monster.name+" 挂了");
			this.setVisible(false);
		}
	},
	_bindHPC:function(){
		this._vampire.HPChangedCallback = this._HPChangedCallback.bind(this);
	},
	fire:function(){
		
		var Ama = [];
		for(var i = 0; i < this._monsterAll.length; i++){
			Ama.push(this._monsterAll[i]._monster);
		}
		var selfA = this;
		this._vampire.fire(Ama, function (attackTarget, attacker, skill, damage) {
			if(damage == 0){
				
				for(var i = 0; i < selfA._monsterAll.length; i++){
					if(selfA._monsterAll[i]._monster == attackTarget){
// selfA._monsterAll[i].runAction(
// cc.Sequence.create(cc.FadeTo.create(0.5, 0))
// );
						selfA._monsterAll[i]._injured();
// cc.log(attackTarget.name+"------");
					}
				}
// cc.log("我擦，怪物超级闪避");
			}
// jdk.log("attackTarget={0} attacker={1} skill={2} damage={3}",
// attackTarget.name, attacker.name, skill.name, damage);
		}, true);
	},
	_createBlood:function(width_b, height_b){
		this._bg = new cc.Sprite(res.all_bg_04_png);
		this._bg.setPosition(width_b - 1, height_b);
		this._bg.setAnchorPoint(0, 0.5);
		this.addChild(this._bg);

		this._b_bg = new cc.Sprite(res.all_bg_03_png);
		this._b_bg.setPosition(width_b, height_b);
		this._b_bg.setAnchorPoint(0, 0.5);
// this.b_bg.setScaleX(1);
		this.addChild(this._b_bg);
	},
	_setReduceBlood:function(num){
		this._b_bg.setScaleX(num);
		var act1 = cc.FadeTo.create(0.3, 100);
		var act2 = cc.FadeTo.create(0.3, 255);
		var sq1 = cc.Sequence.create(act1,act2);
		this._b_bg.runAction(sq1);
	},
	_createCD:function(cdp, width_b, height_b){
	
		cdp.setPosition(width_b + 180, height_b - 200);
		cdp.setAnchorPoint(0, 0.5);
		this.addChild(cdp);
	},
	_setReduceCD:function(cd, num){
		cd.setScaleX(num);
	},
	_createImg:function(width_b, height_b){

		this._img_bg = new cc.Sprite(res.img_jpg);
		this._img_bg.setPosition(width_b + 90, height_b - 100);
		this.addChild(this._img_bg);
	},
	_injured:function(){
		var act1 = cc.FadeTo.create(0.3, 100);
		var act2 = cc.FadeTo.create(0.3, 255);
		var sq1 = cc.Sequence.create(act1,act2);
		this._img_bg.runAction(sq1);
	},
	_putInjured:function(){
		var act1 = cc.ScaleTo.create(0.1, 0.90);
		var act2 = cc.ScaleTo.create(0.1, 1);
		var act3 = cc.ScaleTo.create(0.1, 0.90);
		var act4 = cc.ScaleTo.create(0.1, 1);
		var sq1 = cc.Sequence.create(act1, act2, act3, act4);
		this._img_bg.runAction(sq1);
	},
	init:function(MA){
		this._monsterAll = MA;
		
		this._bindDTCC();
		this._bindHPC();
	}
});


var fightLayerScene = cc.Scene.extend({
	enemy_num:null,
	ctor:function (enemy_num) {
		this._super();
		this.enemy_num = enemy_num;
	},
    onEnter:function () {
        this._super();
        var cslayer = new fightLayer(this.enemy_num);
        this.addChild(cslayer);

// this.ALayer.setVisible(false);
    }
});

