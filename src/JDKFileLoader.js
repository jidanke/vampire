/**
 * Created by Alex on 2015/2/3.
 * 注册自定义的Json Loader：
 * 1、在JSB模式下重定义cc.loader的Json Loader，增加载入前的文件md5校验；
 * 2、提供jdk.jsonLoader，支持json文件缓存和JSB下文件md5值的注册。
 *
 * @example
 * <p>
 *     //注册自定义的Json Loader
 *     jdk.registerJsonLoader();
 *     var jsonFileName = "res/json_data.json";
 *
 *     //指定载入指定json文件时检查其md5值，jsb下有效
 *     jdk.jsonLoader.addMD5Check(jsonFileName, "b1656e86a7b4828acc774cefd4e06871");
 *     //载入json文件
 *     jdk.jsonLoader.load(jsonFileName, function (err, data) {
 *         if (!err && data) {
 *             jdk.log("json:{0}", JSON.stringify(data));
 *             }
 *         });
 *     //检查json文件是否被缓存
 *     jdk.log("{0} is cached: {1}.", jsonFileName, jdk.jsonLoader.isCached(jsonFileName));
 *     //释放json文件缓存
 *     jdk.jsonLoader.release(jsonFileName);
 *     jdk.log("{0} is cached: {1}.", jsonFileName, jdk.jsonLoader.isCached(jsonFileName));
 *
 *     jdk.jsonLoader.load("res/json_data.json", function (err, data) {
 *         if (!err && data) {
 *             jdk.log("json:{0}", JSON.stringify(data));
 *         }
 *     });
 *     jdk.log("{0} is cached: {1}.", jsonFileName, jdk.jsonLoader.isCached(jsonFileName));
 *     </p>
 */

var jdk = jdk || {};

/**
 * JDK自定义的Json Loader，默认为null，需要执行函数jdk.registerJsonLoader()来注册方有效。
 * @type {null}
 */
jdk.jsonLoader = null;

/**
 * 注册JDK自定义的JSONLoader到cc.loader
 * @return {object}
 */
jdk.registerJsonLoader = function () {
    if (jdk.jsonLoader) {
        return jdk.jsonLoader;
    }

    jdk.jsonLoader = (function () {
        var md5Register = {};

        /**
         * 若是JSB模式注册自定义的jsonloader，加载文件前检查文件的MD5
         */
        if (cc.sys.isNative) {
            require("src/md5.js");
            cc.loader.register(["json"], {
                /**
                 *  校验给定url文件的MD5值。
                 * @param url
                 * @param data
                 * @returns {boolean}
                 * @private
                 */
                _md5Check: function (url, data) {
                    if (data && md5Register[url]) {
                        return String(CryptoJS.MD5(data)).toLowerCase() == md5Register[url];
                    }

                    return true;
                },

                /**
                 * 加载指定url的文件
                 * @param realUrl
                 * @param url
                 * @returns {*}
                 */
                load: function (realUrl, url) {
                    var data = jsb.fileUtils.getStringFromFile(realUrl);
                    try {

                        if (!this._md5Check(url, data)) {
                            throw "The md5 verification fails: " + url;
                        }

                        return JSON.parse(data);
                    } catch (e) {
                        cc.error(e);
                        return null;
                    }
                }
            });
            jdk.log("The inner json loader is registered successfully for jdk jsb.")
        }

        return {
            /**
             * Json文件缓存
             */
            _jsonCache: {},

            /**
             * 指定在载入指定url的文件时校验其MD5值
             * @param url
             * @param md5
             */
            addMD5Check: function (url, md5) {
                if (cc.sys.isNative) {
                    md5Register[url] = md5.toLowerCase();
                }
            },

            /**
             * 不校验指定url文件的MD5值
             * @param url
             */
            removeMD5Check: function (url) {
                if (cc.sys.isNative && url in md5Register) {
                    delete md5Register[url];
                }
            },

            /**
             * 载入指定文件，若文件已经在缓存中则使用缓存数据
             * @param url
             * @param cb
             */
            load: function (url, cb) {
                var self = this;
                if (self._jsonCache[url]) {
                    jdk.log("Cache hit, file `{0}`.", url);
                    cb && cb(null, self._jsonCache[url]);
                } else {
                    /**
                     * @note 由于JSB cc.loader.loadJson实现直接调用的是cc.loader.loadTxt,
                     * 而不是注册的json loader。使用cc.loader.loadJson加载将绕过md5校验。
                     */
                    cc.loader.load(url, function (err, data) {
                        var jo = data instanceof Array ? data[0] : data;
                        if (!err) {
                            self._jsonCache[url] = jo;
                            jdk.log("File `{0}` load completed.", url);
                        }
                        cb && cb(err, jo);
                    });
                }
            },

            /**
             * 获取指定文件
             * @param url
             * @returns {class}
             */
            getJson: function (url) {
                if (!this._jsonCache[url]) {
                    var data = cc.loader.getRes(url);
                    if (data) {
                        this._jsonCache[url] = data;
                    }
                }

                return this._jsonCache[url];
            },

            /**
             * 指定url的json文件是否在缓存中
             * @param url
             * @returns {boolean}
             */
            isCached: function (url) {
                return url in this._jsonCache;
            },

            /**
             * 清理指定url的缓存
             * @param url
             */
            release: function (url) {
                if (url in this._jsonCache) {
                    delete this._jsonCache[url];
                    jdk.log("Remove file `{0}` from cache.", url);
                }
            },

            /**
             * 清理所有缓存的json文件
             */
            releaseAll: function () {
                this._jsonCache = {};
                jdk.log("Remove all files from cache.");
            }
        };
    })();

    return jdk.jsonLoader;
};
