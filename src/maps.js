/**
 * Created by bamboo on 2015/3/20.
 */
var maps = maps || {};

maps.Resources = cc.Class.extend({
	_tiled: null,
	_tiledObj:null,
	_resourcesArr:[],
	_x:49,
	_y:49,
	_stepCount: 0,
	_stepRandomCb: null,
	_crossedArray: [],
	_b_num:null,
	ctor:function(res, stepRandomCb, initConfigArray, crossedArray, b_num) {
		this._tiled = new cc.TMXTiledMap(res);
		this._stepRandomCb = stepRandomCb;
		this._crossedArray = crossedArray;
		this._traverseResources(initConfigArray);
		this._b_num = b_num;
	},

	getTiled:function(){
		return this._tiled;
	},
	_traverseResources:function(obj){
		this._toTwoArray("50");
		for(var k in obj){
			var item = obj[k];
			var tiledObj = this._tiled.getObjectGroup(item.name);
			for(var j in tiledObj.getObjects()){
				var x = tiledObj.getObjects()[j]['x']/32;
				var y = tiledObj.getObjects()[j]['y']/32;
				cc.log(x+"--"+y+obj[k]);
				this._resourcesArr[x][y] = new item.cls(x, y, item.cb, this);
			}			
		}

		for(var p in this._crossedArray){
			var x = this._crossedArray[p][0];
			var y = this._crossedArray[p][1];
			this._resourcesArr[x][y].isCrossed = true;
		}
	},

	_toTwoArray:function(num){
		for(var i=0;i<num;i++){
			this._resourcesArr[i] = []; 
			for(var j=0;j<num;j++){
				this._resourcesArr[i][j] = new NullClass(i, j, null, this);
			}
		}
	},

	addCrossedCell: function(x, y){
		this._crossedArray.push([x, y]);
	},

	getCellObj: function(x, y) {
		if(this.isOutOfRange(x, y)){
			return false;
		}
		return this._resourcesArr[x][y];
	},
	
	isOutOfRange: function(x, y){
		return x < 0 || x > this._x || y < 0 || y > this._y;
	},
	
	isCanMove: function(x, y){
		if(this.isOutOfRange(x, y)){
			return false;
		}
		
		return this._resourcesArr[x][y].canMove;
	},
	incStep: function(x, y){
		
		cc.log(this._stepCount);
		
		if(this._stepCount < 15){
			var mr = parseInt(Math.random()*100);
			if(mr < this._b_num[this._stepCount]){
				this.resetStep();
				this._stepRandomCb();
			}
		}else{
			this.resetStep();
			this._stepRandomCb();
		}
		this._stepCount ++;

	},
	resetStep: function(x, y){
		this._stepCount = 0;
	}

});

//迷雾
maps.Fog = cc.Class.extend({
	_x:49,
	_y:49,
	_tiled: null,
	ctor:function(res) {
		this._tiled = new cc.TMXTiledMap(res);
	},
	setFog:function(x, y){

		var layer_bb = this._tiled.getLayer("bb");
		for(var i = -1;i <= 1;i++){
			var nx = x - i;
			if(nx < 0 ){
				nx = 0;
			}else if(nx > this._x){
				nx = this._x;
			}
			for(var j = -1;j <= 1;j++){
				var ny = y - j;
				if(ny < 0 ){
					ny = 0;
				}else if(ny > this._y){
					ny = this._y;
				}
				var sb = layer_bb.getTileAt(cc.p(nx, ny));
				if(sb){
					layer_bb.removeChild(sb);
				}
			}
		}
	},
	getTiled:function(){
		return this._tiled;
	}
});


//母体
var objectClass = cc.Class.extend({
	x: null,
	y: null,
	cb: null,
	owner: null,
	canMove: true,
	isCrossed: false,
	_index:0,
	ctor: function(x, y, cb, owner){
		this.x = x;
		this.y = y;
		this.cb = cb;
		this.owner = owner;
	},
	
	_loadConfig:function(){
		
	},
	
	doSomething:function(){
		if(this.isCrossed && this._index == 0){
			this._index ++;
		}
		
		
		if(this.cb){
			if(this.cb[this._index]){
				this.cb[this._index](this);
			}else{
				this.cb[this.cb.length - 1](this);
			}
			
			this._index ++;
		}
		
		if(!this.isCrossed){
			this.owner.addCrossedCell(this.x, this.y);
			this.owner.resetStep(this.x , this.y);
		}		
		this.isCrossed = true;
	}
});

//普通点
var NullClass = objectClass.extend({ 
	doSomething: function(){
		this.owner.incStep(this.x , this.y);
	}
});

//障碍点
var obstaclesClass = objectClass.extend({
	canMove:false,
	_loadConfig:function(){
		
	},
	
	doSomething: function(){
		cc.log(this.x + "," + this.y + ":obstacles");
		this._super();
	}
});

//战斗母体
var BattleClass = objectClass.extend({
	_loadConfig:function(){

	},
	
	doSomething: function(){
		cc.log(this.x + "," + this.y + ":battle");
		this._super();
//		if(!this._flag){
//			return;
//		}
//
//		if(this.isCrossed){
//			this._super();
//			this._flag = false;
//		}else{
//			this._super();
//		}
//		this.owner.resetStep(this.x , this.y);
	}
});

//随机战斗
var RandomBattleClass = BattleClass.extend({
	_loadConfig:function(){
		//这个是重载哦
	},
	
	doSomething: function(){
		cc.log(this.x + "," + this.y + ":RandomBattleClass");
		this._super();
	}
});

//木材战斗点
var WoodBattleClass = BattleClass.extend({
	_loadConfig:function(){
		//根据x,y读取配置
	}
});

//布料战斗点
var ClothBattleClass = BattleClass.extend({
	_loadConfig:function(){
		//根据x,y读取配置
	}
});



